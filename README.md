# Ansible Project

Collection of Ansible fun

## Getting started

This repo contains 4 files

* base_packs.yml is a playbook to install raspberry pi base packages after new OS deployment
* hosts.json is an example json grouping of hosts
* hosts.yml is an example yml grouping of hosts
* patch.yml is a playbook to see if there are patches needed and determine if a reboot is required from prevous patches
* rpi_config_update.yml is a playbook to update rpi-update and ensure pexpect is installed via pip
* ssh_key_distro.yml adds public keys to the authorized_keys
* rh_base_packs.yml red hat linux example with a typo (apt:)
